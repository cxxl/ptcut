#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

import re
from prt import prt
from sympy import sympify, Symbol
from sym_util import mysgn, logep


# missing: caching

def _symp(exp):
    return sympify(exp) if type(exp) == str else exp

def T_tropicalize_preprocessed(polys, params, vars, ep=0.1, scale=1, sumup=False, term_rescale=False,
        keep_coeff=False):
    polys = [[{sympify(v):_symp(exp) for v,exp in t.items()} for t in f] for f in polys]
    return tropicalize_preprocessed(polys, params, vars, ep, scale, sumup, term_rescale, keep_coeff, quiet=False)

class ZeroTerm(Exception):
    pass

def tropicalize_preprocessed(polys, params, vars, ep, scale=1, sumup=False, term_rescale=False, keep_coeff=False,
        quiet=False):
    """
    input:
    list of list of dict of term-factors with powers,
    dict of parameters,
    list of variables

    term_rescale: if set, the product of parameters is rescaled, rather than each parameters individually

    sumup: if set, after parameter substitution, terms with matching exponents will be collected
    ex: With k1=2, k2=3, k1*x1 + k2*x1 first becomes 2*x1 + 3*x1 and finally 5*x1.  Even more dramatic,
    if k1 == k2, then k1*x1 - k2*x1 will be 0!  Satya doesn't want that, so we usually use sumup == False.

    keep_coeff: an constant factor (other than -1) is usually ignore in tropical.
    I.e., Trop((a + b)^2) == Trop(a^2 + 2ab + b^2) because of that.

    # Simple example: -k1*x1 + k4*x2
    >>> T_tropicalize_preprocessed([[{-1: 1, "k1": 1, "x1": 1}, {"k4": 1, "x2": 1}]], {"k1": 10, "k4": 0.1}, ['x1', 'x2'])
    [{(-1, 1, 0): -1, (1, 0, 1): 1}]

    # With exponents: -k1*x1^4 + k4*x2^2
    >>> T_tropicalize_preprocessed([[{-1: 1, "k1": 1, "x1": 4}, {"k4": 1, "x2": 2}]], {"k1": 10, "k4": 0.1}, ['x1', 'x2'])
    [{(-1, 4, 0): -1, (1, 0, 2): 1}]

    # With exponents: k1^4*x1^2
    >>> T_tropicalize_preprocessed([[{"k1": 4, "x1": 2}]], {"k1": 10}, ['x1'])
    [{(-4, 2): 1}]

    # Absolute values are ignored by default: -10*k1*x1 + k4*x2
    >>> T_tropicalize_preprocessed([[{-10: 1, "k1": 1, "x1": 1}, {"k4": 1, "x2": 1}]], {"k1": 10, "k4": 0.1}, ['x1', 'x2'])
    [{(-1, 1, 0): -1, (1, 0, 1): 1}]

    # Absolute values are used if keep_coeff=True: -10*k1*x1 + k4*x2
    >>> T_tropicalize_preprocessed([[{-10: 1, "k1": 1, "x1": 1}, {"k4": 1, "x2": 1}]], {"k1": 10, "k4": 0.1}, ['x1', 'x2'], keep_coeff=True)
    [{(-2, 1, 0): -1, (1, 0, 1): 1}]

    # Parameters are rescaled individually by default: k1*k2*x1
    >>> T_tropicalize_preprocessed([[{"k1": 1, "k2": 1, "x1": 1}]], {"k1": 3, "k2": 3}, ['x1'])
    [{(0, 1): 1}]

    # Individual rescaling is done before powering: k1^3*x1
    >>> T_tropicalize_preprocessed([[{"k1": 1, "x1": 1}]], {"k1": 3}, ['x1'])
    [{(0, 1): 1}]

    # Parameters are multiplied and then rescaled if term_rescale=True: k1*k2*x1
    >>> T_tropicalize_preprocessed([[{"k1": 1, "k2": 1, "x1": 1}]], {"k1": 3, "k2": 3}, ['x1'], term_rescale=True)
    [{(-1, 1): 1}]

    # Term rescaling is done after powering: k1^3*x1
    >>> T_tropicalize_preprocessed([[{"k1": 3, "x1": 1}]], {"k1": 3}, ['x1'], term_rescale=True)
    [{(-1, 1): 1}]

    # Parameters in exponents are allowed: k1^k2*x1
    >>> T_tropicalize_preprocessed([[{"k1": "k2", "x1": 1}]], {"k1": 10, "k2": 3}, ['x1'], term_rescale=True)
    [{(-3, 1): 1}]

    # Terms with a zero are ignored: k1*x1 + k2*x2, k1=0
    >>> T_tropicalize_preprocessed([[{"k1": 1, "x1": 1}, {"k2": 1, "x2": 1}]], {"k1": 0, "k2": 1}, ['x1', 'x2'])
    Term in equation 1 is zero since k1=0
    [{(0, 0, 1): 1}]
    """
    assert not sumup        # not yet supported
    trop = []
    # cycle over all formulas
    for cnt,f in enumerate(polys, 1):
        d = {}
        # cycle over all terms of a formula
        for t in f:
            try:
                sign = 1
                tv = [0] * (len(vars) + 1)
                # if term_rescale, we multiply the parameters in param_prod and calc the log at the end.
                # if not, we add the log's of the individiual parameters to tv[0].
                param_prod = 1
                # cycle over all v,exp in a term
                for v,exp in t.items():
                    # handle exp if it's symbolic
                    try:
                        if exp.free_symbols:
                            # replace parameters by values
                            exp = exp.subs(params)
                        assert not exp.free_symbols
                    except AttributeError:
                        pass

                    if v.is_constant():
                        # constant
                        # functions with constant arguments are handled here as well.
                        # the conversion to float happens before the call to logep().
                        # functions handled: log, ln  (log is ln)
                        assert exp == 1
                        sign = mysgn(v)
                        if keep_coeff:
                            #import math
                            #print(math.log(v * sign, ep))
                            if term_rescale:
                                param_prod *= sign * v
                            else:
                                tv[0] += logep(sign * v, ep, scale)     # returns int
                    else:
                        # symbol, or even expression
                        sv = str(v)
                        if sv in vars:
                            # single variable name
                            i = vars.index(sv) + 1
                            tv[i] = round(exp * scale)
                        else:
                            # parameter name, possibly expression of parameters
                            if v in params:
                                # single parameter name
                                p = params[v]
                            else:
                                # expression of parameters
                                p = sympify(v).subs(params)
                            if p == 0:
                                if not quiet:
                                    prt("Term in equation {} is zero since {}=0".format(cnt, v))
                                raise ZeroTerm
                            if p < 0:
                                # evil: negative constant!
                                sign *= -1
                                p *= -1
                            assert p > 0, "eq {}: t={}, v={}, exp={}, p={}".format(cnt, t, v, exp, p)
                            if term_rescale:
                                param_prod *= p ** exp
                            else:
                                tv[0] += round(logep(p, ep, scale) * exp)       # exp might be float, so round again
                if term_rescale:
                    tv[0] = logep(param_prod, ep, scale)

                k = tuple(tv)                               # must convert to immutable tuple
                if k in d:
                    if d[k] != sign:
                        # if we have two tropical points with different signs, save the sign as zero.
                        # this will disable opposite sign checking for this point.
                        d[k] = 0
                else:
                    d[k] = sign
            except ZeroTerm:
                pass
        if d:
            trop.append(d)
        elif not quiet:
            prt("Equation {} is zero, ignored.".format(cnt))

    return trop



def T_tropicalize_formulas(str_formulas, params, ep=0.1, scale=1, mul_denom=False, verbose=0, quiet=True,
        sumup=False, term_rescale=False, keep_coeff=False):
    trop, vars, numers = tropicalize_formulas(str_formulas, params, ep, scale, verbose, quiet, mul_denom, sumup, term_rescale, keep_coeff)
    return [sorted(d.items()) for d in trop], vars

def tropicalize_formulas(str_formulas, params, ep, scale=1, verbose=0, quiet=False, mul_denom=False,
        sumup=False, term_rescale=False, keep_coeff=False):
    """
    >>> d = {"k1": 10, "k2": 100, "k3": 3}

    # Simple example
    >>> s = ["k2*x2 - k1*x1"]
    >>> T_tropicalize_formulas(s, d)
    ([[((-2, 0, 1), 1), ((-1, 1, 0), -1)]], ['x1', 'x2'])

    # Exponents are supported
    >>> s = ["k2*k1*x2 - k1*x1**2"]
    >>> T_tropicalize_formulas(s, d)
    ([[((-3, 0, 1), 1), ((-1, 2, 0), -1)]], ['x1', 'x2'])

    # Parameters in exponents work
    >>> s = ["k2**k3*x2 - k1*x1**k3"]
    >>> T_tropicalize_formulas(s, d, quiet=False)
    [1]
    System is exponential in parameter k3.
    ([[((-6, 0, 1), 1), ((-1, 3, 0), -1)]], ['x1', 'x2'])

    # Variables in exponents are not allowed
    >>> s = ["2**x2"]
    >>> T_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    pre_tropicalize.ExponentialFormulaException: ('System is exponential in variable', ['x2'])

    # Rational in variable
    >>> s = ["x2/x1 + x1/x2"]
    >>> T_tropicalize_formulas(s, d, quiet=False, mul_denom=True)
    [1]
    Multiplying equation 1 with: x1*x2
    System is rational in variable x1,x2.
    ([[((0, 0, 2), 1), ((0, 2, 0), 1)]], ['x1', 'x2'])

    # Rational in parameter
    >>> s = ["x2/k1"]
    >>> T_tropicalize_formulas(s, d, quiet=False, mul_denom=True)
    [1]
    System is rational in parameter k1.
    ([[((1, 1), 1)]], ['x2'])

    # Radical in parameter
    >>> s = ["(k1+10*k2)**0.5*x1"]
    >>> T_tropicalize_formulas(s, d, quiet=False, scale=10)
    [1]
    System is radical in parameter k1,k2.
    ([[((-15, 10), 1)]], ['x1'])

    # Radical in a single variable works
    >>> s = ["x1**0.5"]
    >>> T_tropicalize_formulas(s, d, scale=10)
    ([[((0, 5), 1)]], ['x1'])

    # Radical in variables (but more than just a single variable) cannot be handled
    >>> s = ["(x1+x2)**0.5"]
    >>> T_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    pre_tropicalize.FormulaException: Term in equation 1 has root: (x1 + x2)**0.500000000000000

    # Summand with variables (but more than just a single variable) with no constant in exp leads to unspecific error
    >>> s = ["(x1+x2)**k1"]
    >>> T_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    pre_tropicalize.FormulaException: Term is not a monomial
    """
    from pre_tropicalize import pre_tropicalize_formulas, ExponentialFormulaException

    params_set = set(params.keys())
    r = pre_tropicalize_formulas(str_formulas, params_set, verbose, quiet, mul_denom)
    if not quiet:
        if r[2]:
            prt("System is rational in variable {}.".format(",".join(r[2])))
        if r[3]:
            prt("System is rational in parameter {}.".format(",".join(r[3])))
        if r[5]:
            prt("System is exponential in parameter {}.".format(",".join(r[5])))
        if r[6]:
            prt("System is radical in parameter {}.".format(",".join(r[6])))
    if r[4]:
        raise ExponentialFormulaException("System is exponential in variable", r[4])
    vars = [v for v in r[1] if v not in params_set]
    trop = tropicalize_preprocessed(r[0], params, vars, ep, scale, sumup, term_rescale, keep_coeff, quiet=quiet)

    return trop, vars, r[8]



if __name__ == "__main__":
    import doctest
    doctest.testmod()
