#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

import re
from sympy import sympify, Add
from math import floor, log
from fract import Fract


def nlw_open(fname):
    """
    Open for write and use Unix line endings (LF), even under Windows.
    """
    try:
        # this should work under Python 3, but Python 2 doesn't know newline parameter
        return open(fname, "w", newline="\n")
    except TypeError:
        # Python 2 way that will not work for Python 3
        return open(fname, "wb")


# ---------------------------------
# functions for pre_tropicalization
# ---------------------------------

def flat_ordered_terms(f):
    """
    Return a flattened list of terms, i.e., even for nested Add's.

    >>> f = sympify("a+b")
    >>> list(flat_ordered_terms(f))
    [a, b]
    >>> g = sympify("b+c")
    >>> h = Add(f,g,evaluate=False)
    >>> list(flat_ordered_terms(h))
    [a, b, b, c]
    """
    l = []
    for t in f.as_ordered_terms():
        if type(f) == Add:
            l.extend(flat_ordered_terms(t))
        else:
            l.append(t)
    return l


split_pattern = re.compile(r"\A([a-zA-Z]+)_?([0-9]*)\Z")

def split_name_num(s):
    """
    Split a variable name into "base name" and index.

    >>> split_name_num("x1")
    ('x', 1)
    >>> split_name_num("x_1")
    ('x', 1)
    >>> split_name_num("x0")
    ('x', 0)
    >>> split_name_num("x10")
    ('x', 10)
    >>> split_name_num("x")
    ('x', None)
    >>> split_name_num("$")
    ('$', None)
    """
    m = split_pattern.match(s)
    if not m or not m.group(2):
        return s, None
    return m.group(1), int(m.group(2))


def split_name_num_x(s):
    """
    Make sure the x's are sorted to the beginning when used like this:
    sorted(vars, key=lambda x: split_name_num_x(str(x)))
    """
    r = split_name_num(s)
    return r[0] != "x", r[0], r[1]



# ----------------------------------------------
# functions for substitution and tropicalization
# ----------------------------------------------

inf = float("inf")

def mysgn(x):
    """
    Simple self-made signum function.

    >>> mysgn(12)
    1
    >>> mysgn(-1009912)
    -1
    >>> mysgn(0.001232)
    1
    >>> mysgn(0)
    0
    """
    return -1 if x < 0 else 1 if x > 0 else 0


def logep(x, ep, scale=1):
    """
    >>> logep(3000,0.2)
    -5
    >>> logep(0.0001,0.2)
    6
    >>> logep(0.0001,1/5)
    6
    >>> logep(0.0001,Fract(1,5))
    6
    >>> logep(100,10)
    2
    >>> logep(99,10,10)
    20
    >>> logep(80,10,10)
    19
    """
    assert x > 0
    # improve: use round()
    return floor(log(x,ep) * scale + 0.5)               # floor returns int


int_pattern = re.compile(r"^[-+]?[0-9]+$")
float_int_pattern = re.compile(r"^([-+]?[0-9]+)(?:\.0*)?$")

def bestnum(s):
    """
    Convert string to number.  Use float for broken values and int for integers.
    Use int even for floats with ".0" at the end.

    >>> bestnum("1")
    1
    >>> bestnum("10")
    10
    >>> bestnum("1.1")
    1.1
    >>> bestnum("1.0")
    1
    >>> bestnum("1.")
    1
    >>> bestnum("-1")
    -1
    >>> bestnum("-0001")
    -1
    >>> bestnum("0")
    0
    >>> bestnum("-01.0")
    -1
    >>> bestnum("-1e-2")
    -0.01
    >>> bestnum("1/2")
    0.5
    """
    s = s.strip()
    if int_pattern.match(s):
        return int(s)
    m = float_int_pattern.match(s)
    if m:
        return int(m.group(1))
    return float(Fract(s))


def nice_vars(vars):
    """
    Print list of variables nicely as ranges (x1-x6).
    Input must be already sorted.

    Cave: mixing of variables with and without index doesn't work yet!
    ex: nice_vars(["x", "x1", "x2"]) = "xNone-x2".

    >>> nice_vars(["x1", "x2", "x3", "x6", "k1"])
    'x1-x3, x6, k1'
    >>> nice_vars(["x1", "x2"])
    'x1, x2'
    >>> nice_vars(["x1", "x2", "x3", "x6", "k7", "k8"])
    'x1-x3, x6, k7, k8'
    >>> nice_vars(["x", "y"])
    'x, y'
    """
    import itertools
    r = []
    m = [split_name_num(i) for i in vars]
    for x in itertools.groupby(enumerate(m),key=lambda x: x[1][0] + "." + str(x[0] - (0 if x[1][1] is None else x[1][1]))):
        g = list(x[1])
        if len(g) == 1:
            f = g[0][1]
            r.append(f[0] + ("" if f[1] is None else str(f[1])))
        elif len(g) == 2:
            f = g[0][1]
            l = g[-1][1]
            assert f[0] == l[0]
            r.append(f[0] + str(f[1]) + ", " + f[0] + str(l[1]))
        else:
            f = g[0][1]
            l = g[-1][1]
            assert f[0] == l[0]
            r.append(f[0] + str(f[1]) + "-" + f[0] + str(l[1]))
    return ", ".join(r)



if __name__ == "__main__":
    import doctest
    doctest.testmod()
