#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division
try:
    from sage.all_cmdline import *                      # import sage library
    IS_SAGE = True
except ImportError:
    IS_SAGE = False

import numpy as np
from prt import prt
from phwrapper import phwrap, to_int
from math import log, log10, floor
from util import tame_it, status_print
from sym_util import nlw_open
import fractions
import sys


class PtsBag(list):
    """
    A bag (set) of polyhedra.  Subclassed only so we can add another member.
    """
    def __init__(self, l=[], name=None):
        list.__init__(self, l)
        self.name = "" if name is None else name


def comboname(combo):
    """
    >>> comboname({0:1, 1:3})
    '1-3'
    >>> comboname({1:3})
    'x-3'
    >>> comboname({})
    ''
    """
    s = ""
    if combo:
        for i in range(max(combo.keys()) + 1):
            if s:
                s += "-"
            if i in combo:
                s += str(combo[i])
            else:
                s += "x"
    return s


def prtexp(e):
    if e != 1:
        return "**{}".format(e)
    return ""


def sol_to_lp(p, vars, ep=None, scale=None, bounds=False):
    r"""
    Print a polyhedron in .lp file format.

    >>> p1 = phwrap(eqns=[[1,2,3]])
    >>> sol_to_lp(p1, ["x1","x2"], bounds=True)
    '\\ dimension 1; is not compact\nMAXIMIZE\nSubject To\n  eq0: +2 x1 +3 x2 = -1\nBOUNDS\n  x1 free\n  x2 free\nEND\n'

    >>> p2 = phwrap(eqns=[[-1,2,-3,9]], ieqs=[[5,4,3,3],[-8,-1,5,-2]])
    >>> sol_to_lp(p2, ["x1","x2","x3"], bounds=True)
    '\\ dimension 2; is not compact\nMAXIMIZE\nSubject To\n  ie0: -5 x1 +39 x2 >= 74\n  ie1: +5 x1 +6 x2 >= -8\n  eq0: +2 x1 -3 x2 +9 x3 = 1\nBOUNDS\n  x1 free\n  x2 free\n  x3 free\nEND\n'
    """
    assert len(p.Hrep()[0].vector()) == len(vars) + 1
    si = []
    se = []
    rsi = []
    rse = []
    if ep:
        assert scale
        delta = ep ** (-0.5 / scale) - ep ** (0.5 / scale)  # factor of inaccuracy due to rounding of log
        lep = log10(ep)
        ldelta = log10(delta)
        rep = float(1 / ep)
    if not scale:
        scale = 1
    for h in p.Hrep():
        sx = si if h.is_inequality() else se
        rsx = rsi if h.is_inequality() else rse
        ln = "  {}{}:".format("ie" if h.is_inequality() else "eq", len(sx))
        for i,c in enumerate(h.vector()[1:]):
            if c:
                ln += " {}{} {}".format("" if c < 0 else "+", c, vars[i])
        ln += " {} {}".format(">=" if h.is_inequality() else "=", -h.vector()[0])
        sx.append(ln)
        # "lift" solution
        if ep:
            ln = r"\ "[:-1]                           # add trailing backslash
            # find gcd
            coeffs = [c for c in h.vector()[1:] if c != 0]
            gcd = coeffs[0]
            for c in coeffs[1:]:
                gcd = fractions.gcd(gcd, c)
            gcd = abs(gcd)
            # print monomial
            first = True
            for i,c in enumerate(h.vector()[1:]):
                if c:
                    ln += " {}{}{}".format("" if first else "* ", vars[i], prtexp(c // gcd))
                    first = False
            ln += " {} ".format("=" if not h.is_inequality() else "<=")
            ee = h.vector()[0] / gcd
            if 0 and ee > -sys.float_info.max_10_exp / lep:
                # lifted value will be too large
                ln += "1e{}".format(sys.float_info.max_10_exp)
            elif 0 and ee < -sys.float_info.min_10_exp / lep:
                # lifted value will be too small
                ln += "1e{}".format(sys.float_info.min_10_exp)
            else:
                val = rep ** ee
                #rprec = int(floor(ee * lep + ldelta + 1))
                #rprec = 200 if rprec > 200 else -200 if rprec < -200 else rprec
                #val = myround(val, rprec)
                if val < 1e5 and val == int(val):
                    ln += "{:.0f}".format(val)
                else:
                    ln += "{:g}".format(val)
        else:
            ln = ""
        rsx.append(ln)
    s = ""
    if "combo" in p.__dict__:
        s = "\\ combo: {}\n".format(comboname(p.combo))
    s += "\\ dimension {}; is {}compact\n".format(p.dim(), "" if p.is_compact() else "not ")
    if p.has_v:
        if p.is_compact() and p.dim() > 0:
            s += "\\ volume: {}\n".format(p.volume())
        s += "\\ center: {}\n".format(p.center())
        s += "\\ vertices:\n"
        for v in p.vertices_tuple_list():
            s += "\\    {}\n".format(v)
        if p.lines():
            s += "\\ lines:\n"
            for v in p.lines_tuple_list():
                s += "\\    {}\n".format(v)
        if p.rays():
            s += "\\ rays:\n"
            for v in p.rays_tuple_list():
                s += "\\    {}\n".format(v)
    s += "MAXIMIZE\nSubject To\n"
    padlen = max([len(i) for i in si+se]) + 3
    s += "\n".join([(i + " " * (padlen - len(i)) + j).rstrip() for i,j in zip(si+se, rsi+rse)]) + "\n"
    if bounds:
        s += "BOUNDS\n" + "".join(["  {} free\n".format(i) for i in vars])
    s += "END\n"
    return s


def sol_to_lpfile(r, fname1, vars, ep=None, scale=None):
    import os
    file_cnt = 0
    tame = tame_it(2)
    sts = status_print()
    for p in r:
        fname = fname1 + "-sol-{:05d}.lp".format(file_cnt)
        with nlw_open(fname) as f:
            #prt("[{}]".format(file_cnt))
            f.write(sol_to_lp(p, vars, ep=ep, scale=scale))
        file_cnt += 1
        if tame.go():
            sts.print("({})".format(file_cnt))
    # write dummy file if no solutions
    if file_cnt == 0:
        fname = fname1 + "-sol-{:05d}.lp".format(file_cnt)
        with nlw_open(fname) as f:
            print("\\ no solution", file=f)
        file_cnt += 1
    # remove old, maybe still existing files
    while True:
        fname = fname1 + "-sol-{:05d}.lp".format(file_cnt)
        if not os.path.isfile(fname):
            break
        #prt("<{}>".format(file_cnt))
        os.remove(fname)
        file_cnt += 1
        if tame.go():
            sts.print("({})".format(file_cnt))
    sts.print("")


multi_lp_sep = "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"

def sol_to_string(r, vars, ep=None, scale=None):
    s = ""
    file_cnt = 0
    tame = tame_it(2)
    sts = status_print()
    for p in r:
        s += "\n{} file {}\n\n".format(multi_lp_sep, file_cnt)
        s += sol_to_lp(p, vars, ep=ep, scale=scale)
        file_cnt += 1
        if tame.go():
            sts.print("({})".format(file_cnt))
    if not s:
        s = "\\ no solution"
    else:
        s += "\n{} end\n\n".format(multi_lp_sep)
    sts.print("")
    return s


def sol_string_to_one_lpfile(s, fname1):
    fname = fname1 + "-solutions.txt"
    with nlw_open(fname) as f:
        f.write(s)


def combo_cmp(a, b):
    """
    Compare the combo values a and b and return True if a has smaller values.

    >>> a = {0: 1, 1: 2}
    >>> b = {0: 0, 1: 2}
    >>> combo_cmp(a, b)
    1
    >>> combo_cmp(b, a)
    -1
    >>> combo_cmp(a, a)
    0
    >>> b = {0: 1, 1: 3}
    >>> combo_cmp(a, b)
    -1
    >>> combo_cmp(b, a)
    1
    >>> b = {0: 1, 2: 3}
    >>> combo_cmp(a, b)
    -1
    >>> combo_cmp(b, a)
    1
    >>> b = {2: 3}
    >>> combo_cmp(a, b)
    -1
    >>> combo_cmp(b, a)
    1
    >>> combo_cmp(b, b)
    0
    >>> a = {1: 1}
    >>> b = {1: 0}
    >>> combo_cmp(a, b)
    1
    >>> combo_cmp(b, a)
    -1
    """
    mx = max(max(a.keys()), max(b.keys()))
    for i in range(mx+1):
        if i in a and i in b:
            # both are listed.  return True if a is smaller, False if larger, keep going if equal.
            if a[i] != b[i]:
                return -1 if a[i] < b[i] else 1
        elif i in a or i in b:
            # only one is listed.  return True if a is listed, False if b is listed
            return -1 if i in a else 1
    return 0


def canon_repr(p):
    eqns = sorted(p.equalities_list())
    ieqs = sorted(p.inequalities_list())
    return eqns, ieqs


def canonicalize(rs):
    """
    Sort the equations and inequalities of each polyhedron and
    all the polyhedra against each other.
    """
    rs2 = []
    for p in rs:
        eqns, ieqs = canon_repr(p)
        p2 = phwrap(eqns=eqns, ieqs=ieqs)
        try:
            p2.combo = p.combo
        except AttributeError:
            pass
        rs2.append(p2)
    return sorted(rs2, key=lambda x: canon_repr(x))


def compare_solutions(ps, qs):
    """
    Test if two sets of solutions are the same.
    """
    # first, compare space dimension
    if len(ps) > 0 and len(qs) > 0:
        p = next(iter(ps))
        q = next(iter(qs))
        if p.space_dim() != q.space_dim():
            return max(len(ps), len(qs))
    # create a work copy that we can consume
    ps = ps[:]
    qs = qs[:]
    # search each member of p in q.  if found, remove from both
    for p in ps[:]:
        for q in qs[:]:
            if p == q:
                ps.remove(p)
                qs.remove(q)
                break
    # search each member of q in p.  if found, remove from both
    for q in qs[:]:
        for p in ps[:]:
            if p == q:
                ps.remove(p)
                qs.remove(q)
                break
    # what's left now can not be matched
    return max(len(ps), len(qs))


import json

def save_tropical2(vars, ts):
    """
    >>> save_tropical2(["x1","x2"], [{(0,1,2):1}])
    '[\\n  ["x1","x2"],\\n  [\\n    [[[0,1,2],1]]\\n  ]\\n]\\n'
    """
    assert len(next(iter(ts[0].items()))[0])-1 == len(vars)
    # list of variables
    s1 = json.dumps(vars, separators=(',',':'))
    # ts is a list of dicts with tuples of Ints as keys and Ints as values
    ts2 = [sorted((to_int(k),v) for k,v in d.items()) for d in ts]
    s = "".join(["    " + json.dumps(d, separators=(',',':')) + ",\n" for d in ts2])
    if s:
        s = s[:-2] + "\n"
    return "[\n  " + s1 + ",\n  [\n" + s + "  ]\n]\n"

def save_tropical(vars, ts, fname1):
    fname = fname1 + "-trop.txt"
    with nlw_open(fname) as f:
        prt("Saving tropical system...", flushfile=True)
        f.write(save_tropical2(vars, ts))
        prt.flush_all()


def load_tropical2(s):
    """
    >>> load_tropical2('[ ["x1","x2"], [ [[[0,1,2],1]] ] ]')
    (['x1', 'x2'], [{(0, 1, 2): 1}])
    """
    try:
        vars, ts2 = json.loads(s)
    except ValueError:
        return None                             # error while decoding
    # ts is a list of dicts with tuples of Ints as keys and Ints as values
    ts = [{tuple(k): v for k,v in d} for d in ts2]
    return vars, ts

def load_tropical(fname1):
    fname = fname1 + "-trop.txt"
    try:
        with open(fname) as f:
            prt(end="Loading tropical system...", flushfile=True)
            r = load_tropical2(f.read())
            if r is None:
                prt(" error.")
                return None                             # error while decoding
        prt(" done.")
        return r
    except IOError:
        return None                                     # file could not be read


def load_jeff_system(dir):
    fname = dir + "newton.txt"
    ts = []
    cnt = 0
    try:
        with open(fname) as f:
            prt(end="Loading Newton polytopes... ", flushfile=True)
            for l in f:
                l = l.strip()
                if not l:
                    continue
                try:
                    n = json.loads(l)
                    #prt(n)
                    d = {tuple([0] + k): 0 for k in n}
                    #prt(d)
                    ts.append(d)
                    prt(end="[{}]".format(cnt), flush=True)
                except ValueError:
                    prt(" error.")
                    return                              # error while decoding
                cnt += 1
                # ts is a list of dicts with tuples of Ints as keys and Ints as values
        prt(" done.")
        #prt(ts)
        dim = len(next(iter(next(iter(ts)).keys())))
        vars = ["x" + str(i) for i in range(1, dim)]
        return vars, ts
    except IOError:
        return                                          # file could not be read


def rational_str(r):
    return str(r)
    #assert r.denominator() == 1
    #if r.denominator() == 1:
    #    return str(r.numer())
    #return str(r.numer()) + "/" + str(r.denominator())

def list_rational(l):
    return "[" + ",".join([rational_str(i) for i in l]) + "]"

def list_list(l, linestart=""):
    return "[" + (","+linestart).join([list_rational(i) for i in l]) + "]"

def disj_to_str(d, linestart=""):
    if d is None:
        return "None"
    return list_list(d.astype(int).tolist(), linestart="\n "+linestart)

def save_polyhedra(pts_bags, fname1, quiet=False):
    fname = fname1 + "-polyhedra.txt"
    with nlw_open(fname) as f:
        if not quiet:
            prt("Saving polyhedra...", flushfile=True)
        s = "[\n"
        for b in pts_bags:
            s += '  ["{}",\n    '.format(b.name)
            s += disj_to_str(b.qdisj, linestart="    ") + ", [\n"
            s += ",\n".join(["    [" + list_list(p.equalities_list()) + ", " + list_list(p.inequalities_list()) + ((", " + str(p.oidx)) if "oidx" in dir(p) else ", -1") + "]" for p in b])
            s += "\n  ]],\n"
        if s:
            s = s[:-2] + "\n"
        s += "]\n"
        f.write(s)
        if not quiet:
            prt.flush_all()


def load_polyhedra(fname1):
    return load_polyhedra0(fname1 + "-polyhedra.txt")

def load_polyhedra0(fname):
    from ptcut import PtsBag
    try:
        with open(fname) as f:
            prt(end="Loading polyhedra...", flush=True)
            try:
                bags2 = json.loads(f.read())
            except ValueError:
                prt(" error.")
                return None                             # error while decoding
            # bags2 is [name, qdisj, [eqns, ieqs, idx]]
            bags = []
            for bagnb,b2 in enumerate(bags2):
                b = []
                for cnt,p2 in enumerate(b2[2]):
                    p = phwrap(eqns=p2[0], ieqs=p2[1])
                    if p2[2] >= 0:
                        p.idx = p.oidx = p2[2]
                    else:
                        p.idx = cnt
                    p.combo = {bagnb: p.idx}
                    b.append(p)
                b = PtsBag(b)
                b.name = b2[0]
                b.qdisj = np.array(b2[1])
                assert b.name == "#{}".format(bagnb), "{} {}".format(b.name, bagnb)
                bags.append(b)
        prt(" done.")
        return bags
    except IOError:
        return None                                     # file could not be read


if __name__ == "__main__":
    import doctest
    doctest.testmod()
