#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

try:
    from sage.all_cmdline import *                      # import sage library
    IS_SAGE = True
except ImportError:
    IS_SAGE = False


if IS_SAGE:
    # Rational is built-in, thus SAGE will not allow to set attributes
    class Fract(Rational):
        def __init__(self, *args):
            if len(args) == 2:
                super(Fract, self).__init__((args[0], args[1]))
            else:
                try:
                    super(Fract, self).__init__(args[0])
                except TypeError:
                    # stupid Rational has no constructor that takes a decimal as string, like "0.1"
                    super(Fract, self).__init__(float(args[0]))

else:
    from fractions import Fraction
    Fract = Fraction
    Fraction.numer = lambda self: self.numerator
    Fraction.denom = lambda self: self.denominator


def main():
    """
    >>> str(Fract(1,2))
    '1/2'
    >>> str(Fract(0.5))
    '1/2'
    >>> str(Fract("0.1"))
    '1/10'
    >>> f = Fract(1,2)
    >>> f.numer(), f.denom()
    (1, 2)
    >>> str(Fract("1/3"))
    '1/3'
    """

    import doctest
    doctest.testmod()


if __name__ == "__main__":
    main()
