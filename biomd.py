#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

import time
from util import mytime
from sym_util import bestnum, nice_vars
from fract import Fract
from prt import prt


def handle_params(ps):
    """
    Convert parameters from Params.txt file into dictionary.  If possible, the parameters have type int.

    >>> sorted(handle_params(["k1 = 1.2121", "k2 = 13.0", "k3 = 1e-12", "k4 = 19000000", "k5 = 25", "k6 = 0.813", "k7 = 0.557", "k8 = 1/2", "k9 = 100000"]).items())
    [('k1', 1.2121), ('k2', 13), ('k3', 1e-12), ('k4', 19000000), ('k5', 25), ('k6', 0.813), ('k7', 0.557), ('k8', 0.5), ('k9', 100000)]
    """
    d = {}
    for p in ps:
        p = p.split(";")[0]                             # remove comments
        if not p.strip():
            continue
        ar = p.split("=")
        assert len(ar) == 2
        n = ar[1].strip()
        d[ar[0].strip()] = bestnum(n)
    return d


def read_params_file(mod_dir):
    # read parameters from file
    str_params = []
    params = {}
    for fname in ("parameters_q.txt", "parameters_float.txt", "Params.txt"):
        try:
            with open(mod_dir + fname) as f:
                str_params = f.readlines()
                params = handle_params(str_params)
                break
        except FileNotFoundError:
            pass
    return str_params, params


from tropicalize import tropicalize_formulas

def tropicalize_system(mod, mod_dir, ep, params, sumup=True, verbose=0, keep_coeff=False, paramwise=True,
        param_override={}, scale=1, cache=None, mul_denom=False):
    assert not cache            # not supported yet
    # read polynomial system from file
    files = []
    #files = ["numerators.txt"] if mul_denom else []
    files += ["substituted_system_q.txt", "Polynomial_system.txt", "vector_field_q.txt/conservation_laws_q.txt"]
    for basenames in files:
        str_polysys = []
        # read all files, separated by "/"
        for basename in basenames.split("/"):
            try:
                with open(mod_dir + basename) as f:
                    prt("Reading {}".format(mod_dir + basename))
                    str_polysys.extend(f.readlines())
            except FileNotFoundError:
                pass
        if not str_polysys:   # nothing read
            continue
        prt(end="Computing tropical system... ", flush=True, flushfile=True)
        start = mytime()
        trop, vars, numers = tropicalize_formulas(str_polysys, params, ep, scale=scale, verbose=verbose, quiet=False, mul_denom=mul_denom,
            sumup=False, term_rescale=not paramwise, keep_coeff=keep_coeff)
        total = mytime() - start
        prt("Tropicalization time: {:.3f} sec".format(total), flushfile=True)
        prt("Variables: {}".format(nice_vars(vars)))
        return trop, vars, numers

    prt(f"No recognized input file found in directory {mod_dir}")


def read_grid_data(ss):
    """
    >>> read_grid_data("k1:100:200:10")
    [('k1', 100.0, 200.0, 10.0, False)]
    >>> read_grid_data("k10:1.5:2.5:0.1,k11:5:9:1")
    [('k10', 1.5, 2.5, 0.1, False), ('k11', 5.0, 9.0, 1.0, False)]
    >>> read_grid_data("")
    []
    >>> read_grid_data("k1:100:200:*10")
    [('k1', 100.0, 200.0, 10.0, True)]
    >>> read_grid_data("k1:100")
    [('k1', 100.0, 100.0, 1.0, False)]
    """
    r = []
    for s in ss.split(","):
        if s:
            a = s.split(":")
            a2 = a[2] if len(a) > 2 else a[1]
            if len(a) > 3:
                if a[3][0] == "*":
                    mult = True
                    a3 = a[3][1:]
                else:
                    mult = False
                    a3 = a[3]
            else:
                mult = False
                a3 = 1
            r.append((a[0], float(a[1]), float(a2), float(a3), mult))
    return r


def T_sample_grid(grid):
    x = sample_grid(grid)
    return [[j for j in i] for i in x]

def sample_grid(grid):
    """
    >>> T_sample_grid([("k1",1,5,1,False)])
    [[('k1', 1)], [('k1', 2)], [('k1', 3)], [('k1', 4)], [('k1', 5)]]
    >>> T_sample_grid([])
    [[]]
    >>> T_sample_grid([("k1",1,3,1,False), ("k2",6,7,1,False)])
    [[('k1', 1), ('k2', 6)], [('k1', 2), ('k2', 6)], [('k1', 3), ('k2', 6)], [('k1', 1), ('k2', 7)], [('k1', 2), ('k2', 7)], [('k1', 3), ('k2', 7)]]
    >>> T_sample_grid([("k1",1,8,2,True)])
    [[('k1', 1)], [('k1', 2)], [('k1', 4)], [('k1', 8)]]
    >>> T_sample_grid([("k1",1,8,2,True), ("k2",1,10,10,True)])
    [[('k1', 1), ('k2', 1)], [('k1', 2), ('k2', 1)], [('k1', 4), ('k2', 1)], [('k1', 8), ('k2', 1)], [('k1', 1), ('k2', 10)], [('k1', 2), ('k2', 10)], [('k1', 4), ('k2', 10)], [('k1', 8), ('k2', 10)]]
    """
    if not grid:
        yield []
    else:
        vars = [g[0] for g in grid]
        cnt = [g[1] for g in grid]                      # start values
        while True:
            yield list(zip(vars, cnt))                  # make sure it's copied, so the actual values are copied, not their references
            for i in range(len(cnt)):
                if grid[i][4]:
                    assert grid[i][3] != 1
                    cnt[i] *= grid[i][3]                # multiply by step size
                else:
                    assert grid[i][3] != 0
                    cnt[i] += grid[i][3]                # increment by step size
                if cnt[i] > grid[i][2]:                 # over upper limit?  (inclusive!)
                    cnt[i] = grid[i][1]
                else:
                    break                                # values ok, exit loop
            else:
                break                                    # all positions covered, exit outer loop


# less than 0.5 sec
biomd_fast = sorted([
    "BIOMD0000000108", "BIOMD0000000342",                # no solution for eps=1/5
    "BIOMD0000000005c_modified",
    "BIOMD0000000027_transfo", "BIOMD0000000027_transfo_qe", "BIOMD0000000029_transfo", "BIOMD0000000031_transfo",
    "BIOMD0000000035", "BIOMD0000000040", "BIOMD0000000072", "BIOMD0000000077", "BIOMD0000000101", "BIOMD0000000125", "BIOMD0000000150",
    "BIOMD0000000156", "BIOMD0000000159", "BIOMD0000000193", "BIOMD0000000194", "BIOMD0000000198", "BIOMD0000000199",
    "BIOMD0000000233", "BIOMD0000000257", "BIOMD0000000257c",
    "BIOMD0000000289", "BIOMD0000000361", "BIOMD0000000459", "BIOMD0000000460" ])

# more than 0.5 sec
biomd_slow = [ "BIOMD0000000001", "BIOMD0000000002", "BIOMD0000000009", "BIOMD0000000009p", "BIOMD0000000026", "BIOMD0000000026c",
    "BIOMD0000000028", "BIOMD0000000030", "BIOMD0000000038", "BIOMD0000000046",
    "BIOMD0000000080", "BIOMD0000000082", "BIOMD0000000102", "BIOMD0000000122", "BIOMD0000000123",
    "BIOMD0000000163", "BIOMD0000000226", "BIOMD0000000270",
    "BIOMD0000000287" ]

biomd_slowhull = [ "BIOMD0000000001", "BIOMD0000000002", "BIOMD0000000026", "BIOMD0000000028", "BIOMD0000000030", "BIOMD0000000038", "BIOMD0000000046",
    "BIOMD0000000080", "BIOMD0000000082", "BIOMD0000000123", "BIOMD0000000163", "BIOMD0000000270" ]

biomd_hard = [ "BIOMD0000000146_numer", "BIOMD0000000220p", "bluthgen0", "bluthgen1", "bluthgen2" ]
biomd_toohard = [ "BIOMD0000000019", "BIOMD0000000255", "BIOMD0000000335" ]

biomd_simple = sorted(biomd_fast + biomd_slow)
biomd_all = sorted(biomd_simple + biomd_hard)
biomd_easy = sorted(set(biomd_simple) - set(["BIOMD0000000102"]))
biomd_bball = sorted(set(biomd_all) - set(["BIOMD0000000270"]))


def load_known_solution(mod):
    try:
        from _biomdsoldb import biomd_sol_db
    except ImportError:
        return None
    try:
        return biomd_sol_db[mod.upper()]()
    except KeyError:
        return None


if __name__ == "__main__":
    import doctest
    doctest.testmod()
