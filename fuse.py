
# $+HEADER$
#
# Copyright 2017-2018 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

try:
   from sage.all_cmdline import *   # import sage library
except ImportError:
   pass

from util import prt, status_print, tame_it


def fuse_two_polytopes(p, q):
   """
   >>> p = Polyhedron(vertices=[[1,1],[0,0]])
   >>> q = Polyhedron(vertices=[[1,1],[2,2]])
   >>> fuse_two_polytopes(p,q).vertices()
   (A vertex at (0, 0), A vertex at (2, 2))

   >>> p = Polyhedron(vertices=[[1,1],[0,0]])
   >>> q = Polyhedron(vertices=[[1,1],[2,0]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[[1,1],[0,0]])
   >>> q = Polyhedron(vertices=[[1,1],[2,2],[2,1]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[[1,1],[0,0]])
   >>> q = Polyhedron(vertices=[[2,2],[3,3]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[[0,1],[1,0],[1,2]])
   >>> q = Polyhedron(vertices=[[1,0],[2,0],[1,2],[2,2]])
   >>> fuse_two_polytopes(p,q).vertices()
   (A vertex at (0, 1), A vertex at (1, 0), A vertex at (1, 2), A vertex at (2, 0), A vertex at (2, 2))

   >>> p = Polyhedron(vertices=[[0,1],[1,0],[1,2]])
   >>> q = Polyhedron(vertices=[[1,0],[2,0],[1,3],[2,3]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[[0,1,0],[1,0,0],[1,2,0]])
   >>> q = Polyhedron(vertices=[[1,0,0],[2,0,0],[1,2,0],[2,2,0]])
   >>> fuse_two_polytopes(p,q).vertices()
   (A vertex at (0, 1, 0), A vertex at (1, 0, 0), A vertex at (1, 2, 0), A vertex at (2, 0, 0), A vertex at (2, 2, 0))

   >>> p = Polyhedron(vertices=[[0,1,0],[1,0,0],[1,2,0]])
   >>> q = Polyhedron(vertices=[[1,0,0],[2,0,0],[1,3,0],[2,3,0]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[[0,1,0,0,0],[1,0,0,0,0],[1,2,0,0,0]])
   >>> q = Polyhedron(vertices=[[1,0,0,0,0],[2,0,0,0,0],[1,2,0,0,0],[2,2,0,0,0]])
   >>> fuse_two_polytopes(p,q).vertices()
   (A vertex at (0, 1, 0, 0, 0), A vertex at (1, 0, 0, 0, 0), A vertex at (1, 2, 0, 0, 0), A vertex at (2, 0, 0, 0, 0), A vertex at (2, 2, 0, 0, 0))

   >>> p = Polyhedron(vertices=[[0,1,0,0,0],[1,0,0,0,0],[1,2,0,0,0]])
   >>> q = Polyhedron(vertices=[[1,0,0,0,0],[2,0,0,0,0],[1,3,0,0,0],[2,3,0,0,0]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[[0,1],[1,0],[2,1],[1,2]])
   >>> q = Polyhedron(vertices=[[1,0],[0,1],[1,2],[3,2],[3,0]])
   >>> fuse_two_polytopes(p,q).vertices()
   (A vertex at (0, 1), A vertex at (1, 0), A vertex at (1, 2), A vertex at (3, 2), A vertex at (3, 0))

   >>> p = Polyhedron(vertices=[[0,1],[1,0],[2,1],[1,2]])
   >>> q = Polyhedron(vertices=[[1,1],[1,-1],[3,-1],[3,1]])
   >>> fuse_two_polytopes(p,q)

   >>> p = Polyhedron(vertices=[
   ...    (-1, 0, -2, 1, 0, 2, -3, -3, -2, -2, -2, 0, 0),
   ...    (-1, 0, -2, 1, 0, 2, -3, -3, -2, 0, -2, 0, 2),
   ...    (-1, 0, 2, 1, 4, 2, -3, -3, -2, -2, -2, 0, 0),
   ...    (-1, 0, -2, 0, 0, 2, -3, -3, -3, -1, -2, 0, 1),
   ...    (-1, 0, 1, 0, 3, 2, -3, -3, -3, -1, -2, 0, 1),
   ...    (-1, -1, -1, 0, 1, 1, -3, -2, -2, -1, -1, 1, 1),
   ...    (-1, 0, 2, 1, 4, 2, -3, -3, -2, 0, -2, 0, 2),
   ...    (-1, -1, 0, 0, 2, 1, -3, -2, -2, -1, -1, 1, 1),
   ...    (-1, -1, 0, 0, 2, 1, -3, -2, -2, 0, -1, 1, 2),
   ...    (-1, 0, 1, 0, 3, 2, -3, -3, -3, -2, -2, 0, 0),
   ...    (-1, 0, -2, 0, 0, 2, -3, -3, -3, -2, -2, 0, 0),
   ...    (-1, -1, -1, 0, 1, 1, -3, -2, -2, 0, -1, 1, 2),
   ...    (0, 0, -2, 1, 1, 3, -3, -3, -2, -1, -2, 1, 2),
   ...    (0, 0, 0, 0, 3, 3, -3, -3, -3, -2, -2, 1, 1),
   ...    (0, 0, 1, 1, 4, 3, -3, -3, -2, -1, -2, 1, 2),
   ...    (0, 0, -2, 0, 1, 3, -3, -3, -3, -2, -2, 1, 1),
   ...    (0, 0, 1, 1, 4, 3, -3, -3, -2, -2, -2, 1, 1),
   ...    (0, 0, -2, 1, 1, 3, -3, -3, -2, -2, -2, 1, 1),
   ... ])
   >>> q = Polyhedron(vertices=[
   ...    (-1, -1, -2, 0, 0, 1, -3, -2, -2, -2, -1, 1, 0),
   ...    (-1, 0, -2, 0, 0, 2, -3, -3, -3, -3, -2, 0, -1),
   ...    (-1, 0, -2, 1, 0, 2, -3, -3, -2, -2, -2, 0, 0),
   ...    (-1, 0, -2, 0, 0, 2, -3, -3, -3, -2, -2, 0, 0),
   ...    (-1, -1, -1, 0, 1, 1, -3, -2, -2, -1, -1, 1, 1),
   ...    (-1, -1, 0, 0, 2, 1, -3, -2, -2, -2, -1, 1, 0),
   ...    (-1, -1, 0, 0, 2, 1, -3, -2, -2, -1, -1, 1, 1),
   ...    (-1, 0, 1, 0, 3, 2, -3, -3, -3, -3, -2, 0, -1),
   ...    (-1, 0, 2, 1, 4, 2, -3, -3, -2, -2, -2, 0, 0),
   ...    (-1, 0, 1, 0, 3, 2, -3, -3, -3, -2, -2, 0, 0),
   ...    (0, 0, 0, 0, 3, 3, -3, -3, -3, -2, -2, 1, 1),
   ...    (0, 0, 1, 1, 4, 3, -3, -3, -2, -2, -2, 1, 1),
   ...    (0, 0, -2, 0, 1, 3, -3, -3, -3, -2, -2, 1, 1),
   ...    (0, 0, -2, 1, 1, 3, -3, -3, -2, -2, -2, 1, 1),
   ... ])
   >>> fuse_two_polytopes(p,q)
   A 5-dimensional polyhedron in ZZ^13 defined as the convex hull of 18 vertices

   >>> p = Polyhedron(vertices=[
   ...    (-5, 3, 0, -3, -2, 3, -3, -6, -9, 0, 3, 1, -2),
   ...    (-5, 3, 0, -5, -2, 3, -3, -6, -11, 0, 3, 3, -2),
   ...    (-3, 1, -2, -3, -2, 1, -3, -4, -7, -2, 1, 1, -2),
   ...    (-3, 1, 1, -3, 1, 1, -3, -4, -7, 0, 1, 1, 0),
   ...    (-5, 3, 5, -3, 3, 3, -3, -6, -9, 0, 3, 1, -2),
   ...    (-5, 3, 3, -5, 1, 3, -3, -6, -11, 0, 3, 3, -2),
   ...    (-3, 1, 1, -3, 1, 1, -3, -4, -7, -2, 1, 1, -2),
   ...    (-3, 3, 0, -3, 0, 5, -3, -6, -9, 0, 1, 1, 0),
   ...    (-3, 1, 0, -3, 0, 1, -3, -4, -7, 0, 1, 1, 0),
   ... ], rays=[
   ...    (0, 1, 1, 0, 1, 2, 0, -1, -1, 0, 0, 0, 0),
   ... ])
   >>> q = Polyhedron(vertices=[
   ...    (-3, 3, 0, -3, 0, 5, -3, -6, -9, 0, 1, 1, 0),
   ...    (-3, 3, 0, -3, 0, 5, -3, -6, -9, 1, 1, 1, 1),
   ...    (-5, 3, 0, -5, -2, 3, -3, -6, -11, 3, 3, 3, 1),
   ...    (-3, 1, 0, -3, 0, 1, -3, -4, -7, 1, 1, 1, 1),
   ...    (-5, 3, 0, -3, -2, 3, -3, -6, -9, 3, 3, 1, 1),
   ...    (-3, 1, -2, -3, -2, 1, -3, -4, -7, 1, 1, 1, 1),
   ...    (-3, 1, -2, -3, -2, 1, -3, -4, -7, -2, 1, 1, -2),
   ...    (-5, 3, 0, -3, -2, 3, -3, -6, -9, 0, 3, 1, -2),
   ...    (-5, 3, 0, -5, -2, 3, -3, -6, -11, 0, 3, 3, -2),
   ...    (-3, 1, 0, -3, 0, 1, -3, -4, -7, 0, 1, 1, 0),
   ... ])
   >>> fuse_two_polytopes(p,q)
   A 5-dimensional polyhedron in ZZ^13 defined as the convex hull of 11 vertices and 1 ray

   >>> p = Polyhedron(vertices=[
   ...    (0, -1, -3, 0, 0, 2, -2, -3, -3, -3, -3, 0, 0),
   ...    (-1, -2, -3, -1, -1, 0, -3, -2, -3, -3, -2, 0, -1),
   ...    (-1, -2, -3, -1, -1, 0, -3, -2, -3, -2, -2, 0, 0),
   ...    (0, -2, -3, -1, 0, 1, -3, -2, -3, -3, -2, 1, 0),
   ... ], rays=[
   ...    (0, 0, -1, 0, -1, 0, 1, -1, -1, -1, -1, -1, -1),
   ...    (0, -1, -1, 0, -1, -1, 1, 0, 0, -1, -1, -1, -1),
   ...    (0, -1, -2, -1, -2, -1, 1, -1, -2, -2, -1, -1, -2),
   ... ])
   >>> q = Polyhedron(vertices=[
   ...    (-1, -1, -4, 0, -2, 1, -1, -4, -4, -4, -4, -2, -2),
   ...    (-1, -2, -5, -1, -3, 0, -1, -4, -5, -5, -4, -2, -3),
   ...    (-1, -2, -5, -1, -3, 0, -1, -4, -5, -4, -4, -2, -2),
   ...    (-1, -1, -4, 0, -2, 1, -1, -4, -4, -3, -4, -2, -1),
   ...    (0, -1, -4, 0, -1, 2, -1, -4, -4, -4, -4, -1, -1),
   ... ], rays=[
   ...    (0, -1, -1, 0, -1, -1, 1, 0, 0, -1, -1, -1, -1),
   ...    (0, 0, -1, 0, -1, 0, 1, -1, -1, -1, -1, -1, -1),
   ... ])
   >>> fuse_two_polytopes(p,q)
   A 5-dimensional polyhedron in ZZ^13 defined as the convex hull of 6 vertices and 3 rays
   """
   #prt(end="{} {}".format(p.combo, q.combo))
   assert p != q
   # the dimension must be the same, otherwise the inclusion test should have found it and it can't be fused anyway
   if p.dim() != q.dim():
      #prt(" diff dim")
      return None
   # it cannot be a point
   if p.dim() == 0:
      return None
   # the intersection must not be empty, otherwise they don't touch
   c = p & q
   if c.is_empty():
      #prt(" no touch")
      return None
   # the convex hull must not have a higher dimension
   hull = p.convex_hull(q)
   if hull.dim() > p.dim():
      #prt(" orthogonal")
      return None
   if p.dim() == 1:
      # this is just a faster specialization for dim 1
      return hull
   else:
      # most general case
      for i in c.faces(0):
         v = i.vertices()[0]              # the vertex itself
         assert hull.contains(v)          # obvious
         if not p.interior_contains(v) and not q.interior_contains(v):     # point is on the border of both polyhedra
            # same point must be on the border of the hull
            if hull.relative_interior_contains(v):
               return None
      return hull


def fuse_polytopes(b, progress=False):
   """
   >>> l = []
   >>> l.append(Polyhedron(vertices=[[1,1],[0,1]]))
   >>> l.append(Polyhedron(vertices=[[1,1],[0,0]]))
   >>> l.append(Polyhedron(vertices=[[1,1],[2,2]]))
   >>> l.append(Polyhedron(vertices=[[0,0],[0,1]]))
   >>> l.append(Polyhedron(vertices=[[2,2],[3,3]]))
   >>> fuse_polytopes(l)
   >>> for p in l:
   ...   print(p.vertices())
   (A vertex at (0, 1), A vertex at (1, 1))
   (A vertex at (0, 0), A vertex at (3, 3))
   (A vertex at (0, 0), A vertex at (0, 1))
   """
   if progress:
      prt(end="Fusing polytopes... ")
   olen = len(b)
   total = (olen**2 - olen) // 2
   tame = tame_it(2)
   sts = status_print()
   # try all combinations
   i = 0
   while i < len(b):
      # no need to test against anything earlier, since that was already done
      j = i + 1
      while j < len(b):
         if progress and tame.go():
            now = olen * i - (i + 1) * i // 2 + j
            sts.print("({:.1f}%)".format(now / total * 100))
         p, q = b[i], b[j]
         c = fuse_two_polytopes(p, q)
         if c is not None:
            # fusion was successful
            if "combo" in p.__dict__:
               c.combo = p.combo   #+ q.combo      # fake
            b[i] = c                               # replace old element
            del b[j]                               # remove other element
         else:
            j += 1
      i += 1
   sts.print("")
   if progress:
      prt("done.")


if __name__ == "__main__":
   import doctest
   doctest.testmod()
