#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

import re
from prt import prt
from sympy import sympify, Add, Mul, lcm, Symbol, FunctionClass
from sympy.core.function import Function
from sym_util import flat_ordered_terms, split_name_num_x, nlw_open
from collections import defaultdict
import json


def T_flat_powers_dict(t):
    return flat_powers_dict(sympify(t))

def flat_powers_dict(t):
    """
    Split a monomial into bases and exponents.  Make sure the
    bases are single indivisible factors, if possible.

    >>> T_flat_powers_dict("-x")
    {-1: 1, x: 1}
    >>> T_flat_powers_dict("-1/(2*x)")
    {-1/2: 1, x: -1}
    >>> T_flat_powers_dict("x*y**2")
    {x: 1, y: 2}
    >>> T_flat_powers_dict("x/y")
    {x: 1, y: -1}
    >>> T_flat_powers_dict("x**2/y**3")
    {x: 2, y: -3}
    >>> T_flat_powers_dict("(x/y)**3")
    {x: 3, y: -3}
    >>> T_flat_powers_dict("(x/y)**z")
    {x: z, y: -z}
    >>> T_flat_powers_dict("(x**b/y**a)**z")
    {x: b*z, y: -a*z}
    >>> T_flat_powers_dict("x*(x**b/y**a)**z")
    {x: b*z + 1, y: -a*z}
    """
    d = {}
    for v, exp in t.as_powers_dict().items():
        if v.is_constant():
            d[v] = d.get(v, 0) + exp
        else:
            for f2 in v.as_ordered_factors():
                for v2, exp2 in f2.as_powers_dict().items():
                    d[v2] = d.get(v2, 0) + exp*exp2
    return d


class FormulaException(Exception):
    def __init__(self, msg):
        self.message = msg

class VarFormulaException(FormulaException):
    def __init__(self, msg, vars):
        super(VarFormulaException, self).__init__(msg)
        self.vars = vars

class ExponentialFormulaException(VarFormulaException):
    pass

class RationalFormulaException(FormulaException):
    pass

class RadicalFormulaException(VarFormulaException):
    pass


bad_pattern = re.compile(r"\b(?:" + "|".join(["zoo", "nan"]) + r")\b")

def T_pre_tropicalize_formulas(str_formulas, params_set, mul_denom=False):
    r = pre_tropicalize_formulas(str_formulas, params_set, 0, True, mul_denom)
    return [sorted([sorted([(str(j[0]), j[1]) for j in i.items()]) for i in k]) for k in r[0]], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8]

def pre_tropicalize_formulas(str_formulas, params_set, verbose=0, quiet=False, mul_denom=False):
    """
    sympify() a list of formulas.  ignore comments and empty lines.  check for nan and zoo.
    if mul_denom==True, multiply rational functions by principle denom.
    In any case, print if system is rational in variables and/or parameters.

    str_formulas are the formulas, one formula per line (as str).
    params_set is a set of paramter names (as str).

    returns ([0] list of list of dict of term-factors with powers,
             [1] sorted list of vars (as str),
             [2] sorted list of variables in principle denom (as str),
             [3] sorted list of parameters in principle denom (as str),
             [4] sorted list of variables in exponents (as str),
             [5] sorted list of parameters in exponents (as str),
             [6] sorted list of parameters in radicants (as str),
             [7] list of principle denoms (as sympy formula)
             [8] list of expanded numerators (as sympy formula))

    >>> d = set(["k1", "k2", "k3", "k4", "k5", "k6", "k7", "k8", "k9"])

    # Simple example:
    >>> s = ["k4*x2-k1*x1"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('-1', 1), ('k1', 1), ('x1', 1)], [('k4', 1), ('x2', 1)]]], ['x1', 'x2', 'k1', 'k4'], [], [], [], [], [], [1], [-k1*x1 + k4*x2])

    # It understands powers, in both notations:
    >>> s = ["k4*x2**3-k1^2*x1"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('-1', 1), ('k1', 2), ('x1', 1)], [('k4', 1), ('x2', 3)]]], ['x1', 'x2', 'k1', 'k4'], [], [], [], [], [], [1], [-k1**2*x1 + k4*x2**3])

    # Terms are added up:
    >>> s = ["2*k4*x1-k4*x1"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('k4', 1), ('x1', 1)]]], ['x1', 'k4'], [], [], [], [], [], [1], [k4*x1])

    # Terms cancel out symbolically:
    >>> s = ["k4*x1-k4*x1+x2"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('x2', 1)]]], ['x2'], [], [], [], [], [], [1], [x2])

    # Terms are expanded:
    >>> s = ["x1*(k1+k2+x2*(k2-k3))"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('-1', 1), ('k3', 1), ('x1', 1), ('x2', 1)], [('k1', 1), ('x1', 1)], [('k2', 1), ('x1', 1)], [('k2', 1), ('x1', 1), ('x2', 1)]]], ['x1', 'x2', 'k1', 'k2', 'k3'], [], [], [], [], [], [1], [k1*x1 + k2*x1*x2 + k2*x1 - k3*x1*x2])

    # If variables are present in denominators, you get an exception:
    >>> s = ["k4*x1/x3-k3*x2/(x1*k4)"]
    >>> T_pre_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    RationalFormulaException: System is rational!

    # Set mul_denom=True to multiply by principal denominator:
    >>> T_pre_tropicalize_formulas(s, d, mul_denom=True)
    ([[[('-1', 1), ('k3', 1), ('k4', -1), ('x2', 1), ('x3', 1)], [('k4', 1), ('x1', 2)]]], ['x1', 'x2', 'x3', 'k3', 'k4'], ['x1', 'x3'], ['k4'], [], [], [], [x1*x3], [-k3*x2*x3/k4 + k4*x1**2])

    # Only factors that contain variables are put into the LCM:
    >>> s = ["k4*x1/(k1*x1 + 10*k1) + x2"]
    >>> T_pre_tropicalize_formulas(s, d, mul_denom=True)
    ([[[('10', 1), ('x2', 1)], [('k1', -1), ('k4', 1), ('x1', 1)], [('x1', 1), ('x2', 1)]]], ['x1', 'x2', 'k1', 'k4'], ['x1'], ['k1'], [], [], [], [x1 + 10], [x1*x2 + 10*x2 + k4*x1/k1])

    # Terms are reduced, even without mul_denom==True:
    >>> s = ["k4*x3/x3-k3*x2"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('-1', 1), ('k3', 1), ('x2', 1)], [('k4', 1)]]], ['x2', 'k3', 'k4'], [], [], [], [], [], [1], [-k3*x2 + k4])

    # This is more compicated and needs simplify():
    >>> s = ["(x1+x2)**4 / (x1**2+2*x1*x2+x2**2)"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('2', 1), ('x1', 1), ('x2', 1)], [('x1', 2)], [('x2', 2)]]], ['x1', 'x2'], [], [], [], [], [], [1], [x1**2 + 2*x1*x2 + x2**2])

    # Pure constants and parameters are ignored as denoms:
    >>> s = ["k4*x1/k3-k3*x2/(2*k4)+x3/2"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('-1/2', 1), ('k3', 1), ('k4', -1), ('x2', 1)], [('1/2', 1), ('x3', 1)], [('k3', -1), ('k4', 1), ('x1', 1)]]], ['x1', 'x2', 'x3', 'k3', 'k4'], [], ['k3', 'k4'], [], [], [], [1], [-k3*x2/(2*k4) + x3/2 + k4*x1/k3])

    # Use of functions cause an exception:
    >>> s = ["sin(x1)"]
    >>> T_pre_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    FormulaException: Term contains function

    >>> s = ["unknownfunc(x1)"]
    >>> T_pre_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    FormulaException: Term contains function

    # However, Pow() is supported:
    >>> s = ["Pow(x1,2)"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('x1', 2)]]], ['x1'], [], [], [], [], [], [1], [x1**2])

    # Symbols in the exponent:
    >>> s = ["2**x1"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('2', x1)]]], ['x1'], [], [], ['x1'], [], [], [1], [2**x1])

    >>> s = ["x1**k2"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('x1', k2)]]], ['x1', 'k2'], [], [], [], ['k2'], [], [1], [x1**k2])

    >>> s = ["x1**(k2+k3)"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('x1', k2 + k3)]]], ['x1', 'k2', 'k3'], [], [], [], ['k2', 'k3'], [], [1], [x1**k2*x1**k3])

    # Roots, i.e., non-integer exponents, of symbols work:
    >>> s = ["x1**0.5"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('x1', 0.500000000000000)]]], ['x1'], [], [], [], [], [], [1], [x1**0.5])

    >>> s = ["k1**0.5"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('k1', 0.500000000000000)]]], ['k1'], [], [], [], [], [], [1], [k1**0.5])

    # Roots of sums etc. don't work if variables are involved:
    >>> s = ["(x1+x2)**0.5"]
    >>> T_pre_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    FormulaException: Term in equation 1 has root: (x1 + x2)**0.500000000000000

    # Roots of sums etc. do work if it's only params:
    >>> s = ["(k1+k2)**0.5"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('k1 + k2', 0.500000000000000)]]], ['k1', 'k2'], [], [], [], [], ['k1', 'k2'], [1], [(k1 + k2)**0.5])

    # Functions are not allowed:
    >>> s = ["somefunc(x1)"]
    >>> T_pre_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    FormulaException: Term contains function

    >>> s = ["(x2 + somefunc(x1))**k2"]
    >>> T_pre_tropicalize_formulas(s, d)
    Traceback (most recent call last):
      ...
    FormulaException: Term contains function

    # It handles multiple formulas at once:
    >>> s = ["k4*x2-k1*x1", "-k4*x1"]
    >>> T_pre_tropicalize_formulas(s, d)
    ([[[('-1', 1), ('k1', 1), ('x1', 1)], [('k4', 1), ('x2', 1)]], [[('-1', 1), ('k4', 1), ('x1', 1)]]], ['x1', 'x2', 'k1', 'k4'], [], [], [], [], [], [1, 1], [-k1*x1 + k4*x2, -k4*x1])

    # The principle denom is calculated per formula, not per system:
    >>> s = ["k4*x2/x1+x3", "-k4*x1/x2+x3"]
    >>> T_pre_tropicalize_formulas(s, d, mul_denom=True)
    ([[[('k4', 1), ('x2', 1)], [('x1', 1), ('x3', 1)]], [[('-1', 1), ('k4', 1), ('x1', 1)], [('x2', 1), ('x3', 1)]]], ['x1', 'x2', 'x3', 'k4'], ['x1', 'x2'], [], [], [], [], [x1, x2], [k4*x2 + x1*x3, -k4*x1 + x2*x3])
    """
    if quiet:
        verbose = False
#    if cache:
#        vars, used_params, old_params, old_trop = cache
#        vars = set(vars)
#        changed_params = set([k for k in params if params[k] != old_params[k]])
#        #prt(end="{}".format(changed_params))
#    else:
    if 1:
        vars = set()
        used_params = []
    l_polys = []
    polys = []                                          # list of sympy formulas, expanded, but not evaluated/canceled out
    denom_sym_set = set()                               # set of names of all symbols used in any denom (as str)
    is_rational = False
    denom_princes = []
    cnt = 0

    for p in str_formulas:
        p = p.split(";", maxsplit=1)[0].strip()         # remove comments
        if not p.split():                               # ignore empty lines
            continue
        cnt += 1
#        if cache and not (changed_params & used_params[cnt]):
#            # we have a cache and there are no parameter changes from last round in this equation:
#            # use old substituted formula.
#            polys.append(None)                          # dummy; it's not used later anyway
#            continue
        if not quiet:
            prt(end="[{}]".format(cnt), status=True)
        if verbose:
            prt("input: {}".format(p), flush=True)
        if bad_pattern.search(p):
            raise FormulaException("System contains infinities")
        f = sympify(p)                                  # convert string to sympy object
#        if not cache:
#            # save all symbols (as strings) used in that equation
#            used_params.append(set([str(j) for j in f.free_symbols]))
        if verbose:
            prt("sympify into: {}".format(f), flush=True)
        f = f.evalf().simplify().expand()               # evaluate constant functions and expand representation into terms
        vars |= f.free_symbols
        lf = flat_ordered_terms(f)                      # list (!) of terms
        if verbose:
            prt("expand into: {}".format(lf), flush=True)

        # handle denominators
        l_denoms = set()                                # list of denoms
        for t in lf:
            denom = t.as_numer_denom()[1]
            sym_set = set([str(i) for i in denom.free_symbols])
            denom_sym_set |= sym_set
            var_set = sym_set - params_set              # only variables
            if var_set:
                # remove all factors that are parameters, like in "k114*x36 + 10*k114"
                l_fac = []
                # break it first into factors
                for fac in denom.factor().as_ordered_factors():
                    sym_set = set([str(i) for i in fac.free_symbols])
                    if sym_set - params_set:
                        l_fac.append(fac)
                denom = Mul(*l_fac)
                l_denoms.add(denom)
        #if (l_denoms):
        #    print(l_denoms)
        prince_denom = lcm(l_denoms)                    # principal denom
        # ??? improve: check if all terms are negative; if so, multiply by -1.
        if prince_denom == 1:
            denom_princes.append(1)                     # to avoid mpz(1)
        else:
            #prince_denom = prince_denom.factor()
            denom_princes.append(prince_denom)
            is_rational = True
            if mul_denom:
                # multiply list of terms with lcm of denoms
                if not quiet:
                    prt("Multiplying equation {} with: {}".format(cnt, prince_denom))
                new_f = []
                for t in lf:                             # list of terms
                    t = flat_ordered_terms((t * prince_denom).simplify().expand())
                    new_f.extend(t)
                f = Add(*new_f)
                lf = flat_ordered_terms(f)
                del new_f
                if verbose:
                    prt("after mul_denom: {}".format(f), flush=True)
        l_polys.append(lf)
        polys.append(f)

    # check for rational
    denom_vars = sorted(denom_sym_set - params_set, key=lambda x: split_name_num_x(x))
    denom_parms = sorted(denom_sym_set & params_set, key=lambda x: split_name_num_x(x))
    if is_rational and not mul_denom:
        raise RationalFormulaException("System is rational!")

    # make sure terms are polynomial
    all_lists = []
    exp_sym_set = set()                                 # set of symbols in exponents (as str)
    radicant_parms = set()
    for f in l_polys:
        # process terms
        term_list = []
        for t in f:
            sign = 1
            d = flat_powers_dict(t)
            term_list.append(d)
            for v, exp in d.items():
                exp_sym_set |= set([str(i) for i in exp.free_symbols])
                assert exp.free_symbols or exp.is_constant()   # if no free symbols, it must be constant
                # either v is constant or a single symbol with no args
                if not v.is_constant():
                    #if isinstance(v.func, FunctionClass):
                    if v.atoms(Function):
                        raise FormulaException("Term contains function")
                    if v.func != Symbol:
                        # if v has only params as free symbols, that might still be ok
                        sym_set = set([str(i) for i in v.free_symbols])
                        if sym_set - params_set:
                            if exp.is_constant():
                                raise FormulaException("Term in equation {} has root: ({})**{}".format(cnt, v, exp))
                            raise FormulaException("Term in equation {} is not a monomial, contains: ({})**{}".format(cnt, v, exp))
                            #raise FormulaException("Term is not a monomial")
                        else:
                            radicant_parms |= sym_set
        all_lists.append(term_list)

    # check for exponential and roots
    exp_vars = sorted(exp_sym_set - params_set, key=lambda x: split_name_num_x(x))
    exp_parms = sorted(exp_sym_set & params_set, key=lambda x: split_name_num_x(x))
    radicant_parms = sorted(radicant_parms & params_set, key=lambda x: split_name_num_x(x))

    # sort symbols alphabetically with the variables in front
    vars = sorted([str(i) for i in vars], key=lambda x: split_name_num_x(x))

    return ( all_lists, vars, denom_vars, denom_parms, exp_vars, exp_parms, radicant_parms, denom_princes, polys )


def T_save_numers2(numers):
    return save_numers2([sympify(f) for f in numers])

def save_numers2(numers):
    """
    #>>> T_save_numers2(["k4*x2 + x1*x3 -k4*x1 + x2*x3","x1**k2*x1**k3"])
    #'[\\n    "-k4*x1 + k4*x2 + x1*x3 + x2*x3",\\n    "x1**k2*x1**k3"\\n]\\n'
    """
    #s = json.dumps([str(f) for f in numers]).replace(", ", ",\n    ")
    #assert s[0] == "["
    #assert s[-1] == "]"
    #s = "[\n    " + s[1:-1] + "\n]\n"
    s = "\n".join([str(f) for f in numers]) + "\n"
    return s

def save_numers(numers, fname):
    with nlw_open(fname) as f:
        prt(end="Saving expanded numerators...")
        f.write(save_numers2(numers))
        prt(" Done.")


def testit():
    import doctest
    doctest.testmod(verbose=False)


if __name__ == "__main__":
    #d = set(["k1", "k2", "k3", "k4", "k5", "k6", "k7", "k8", "k9"])
    #s = ["k4*x2-k1*x1"]
    #print(pre_tropicalize_formulas(s, d))

    testit()
