
# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

# pure python

from __future__ import print_function, division

from prt import prt
from util import tame_it, status_print, mytime
import numpy as np


def is_connected(p, q, con_type):
    c = p & q
    if c.is_empty():
        return False
    # there is some connection
    if con_type == 0:
        return True
    if con_type == 2:
        return p.dim() == q.dim()
    mindim = min(p.dim(), q.dim())
    if mindim == 0:
        return True
    return c.dim() >= mindim - 1


def _connected_components(b, is_con, dbg=False, status=False):
    """
    find the number of connected components.
    originally used an idea from SO: https://stackoverflow.com/questions/4005206/algorithm-for-counting-connected-components-of-a-graph-in-python
    that uses the idea of disjoint sets: https://en.wikipedia.org/wiki/Disjoint-set_data_structure
    fused that with the connection check so that we don't need to compute all intersections.
    This function allows for easy testing.

    >>> def con(a,x,y):
    ...     a[x,y] = 1
    ...     a[y,x] = 1
    >>> class myint(int):
    ...     pass
    >>> def T_connected_components(a):
    ...     return _connected_components([myint(i) for i in range(a.shape[0])], lambda i,j: a[i,j])
    >>> a = np.zeros((0,0), dtype=bool)
    >>> T_connected_components(a)
    0
    >>> a = np.array([[False, False], [False, False]])
    >>> T_connected_components(a)
    2
    >>> a = np.zeros((2,2))
    >>> T_connected_components(a)
    2
    >>> a = np.zeros((2,2))
    >>> con(a,0,1)
    >>> T_connected_components(a)
    1
    >>> a = np.zeros((3,3))
    >>> con(a,0,1)
    >>> T_connected_components(a)
    2
    >>> a = np.zeros((6,6))
    >>> con(a,0,1)
    >>> con(a,1,2)
    >>> con(a,3,4)
    >>> con(a,4,5)
    >>> T_connected_components(a)
    2
    >>> a = np.zeros((6,6))
    >>> con(a,0,2)
    >>> con(a,1,2)
    >>> con(a,3,5)
    >>> con(a,4,5)
    >>> T_connected_components(a)
    2
    >>> a = np.zeros((10,10), dtype=int)
    >>> con(a,0,1)
    >>> con(a,1,2)
    >>> con(a,2,5)
    >>> con(a,3,4)
    >>> con(a,4,7)
    >>> con(a,7,8)
    >>> con(a,5,8)
    >>> T_connected_components(a)
    3
    """
    # init
    if __debug__:
        for i in range(len(b)):
            b[i].idx = i
        ss = set()
        if dbg:
            icnt = 0
    if status:
        total = (len(b)**2 - len(b)) // 2
        tame = tame_it(2)
        sts = status_print()
    lists = [[i] for i in b]         # first, each node is by itself
    # walk the graph
    i = 0
    while i < len(lists):
        # walk through all members of lists[i] and see if they are connected to any of the coming lists[j][0]
        k = 0
        while k < len(lists[i]):
            j = i + 1
            while j < len(lists):
                assert len(lists[j]) == 1
                r = is_con(lists[i][k], lists[j][0])
                if __debug__:
                    # make sure that no connection check is done twice
                    pp = tuple(sorted((lists[i][k].idx, lists[j][0].idx)))
                    assert pp not in ss
                    ss.add(pp)
                    if dbg:
                        print("i={}, j={}, k={}, testing {} & {} = {}".format(i, j, k, lists[i][k].idx, lists[j][0].idx, r))
                        icnt += 1
                if r:
                    # yes!  now move lists[j][0] to lists[i] and remove lists[j]
                    lists[i].append(lists[j][0])
                    del lists[j]
                else:
                    j += 1
            if status and tame.go():
                sts.print("i={}, k={}, j={}".format(i, k, j))
            k += 1
        i += 1
    if __debug__ and dbg:
        prt("total {} intersections".format(icnt))
    if status:
        sts.print("")
    return len(lists)


def connected_components(b, con_type=0, dbg=False, status=False):
    """
    Find number of connected components.
    This is just a wrapper to be used with a list of polyhedra as parameter.
    """
    return _connected_components(b, lambda p,q: is_connected(p,q,con_type), dbg=dbg, status=status)


def build_graph(b, con_type, dbg=False):
    """
    build adjacency matrix of a bag of polytopes.
    an entry is True, iff the two polytopes are connected (i.e. the intersection is not empty)
    """
    # init
    import itertools
    s = ""
    l = len(b)
    adj = np.zeros((l, l), dtype=bool)
    for i,p in enumerate(b):
        p.idx = i
    # test two nodes for connection
    for p,q in itertools.combinations(b, 2):
        c = p & q
        if is_connected(p, q, c, con_type):
            if dbg:
                prt("Connection between {} and {}, dim={}/{}/{}".format(p.idx, q.idx, p.dim(), q.dim(), c.dim()))
            adj[p.idx, q.idx] = True
            adj[q.idx, p.idx] = True
            s += "{}:{}:{}/{}/{}\n".format(p.idx, q.idx, p.dim(), q.dim(), c.dim())
    if dbg:
         for p in b:
             if not any(adj[p.idx,:]):
                 prt("No connection to {}, dim={}".format(p.idx, p.dim()))
    return adj, s


def graph_name(s):
    """
    Return a (more or less) unique name for a graph.
    Hash it and return the first 12 digits in base 36.
    The chance of collision is 1 in 36**12**0.5 ~= 2e9

    #>>> graph_name("x")
    #'14RTYH2WEZM8'
    """
    import hashlib
    return np.base_repr(int(hashlib.sha256(s).hexdigest(), 16), 36)[:12]


def main(files, dbg=True, status=False, props_prefix=None):
    import lp2pt
    b = lp2pt.load_ph_from_fname_list(files)
    prt("Loaded {} polyhedra".format(len(b)))
    con_type = 0
    t = mytime()
    cc = connected_components(b, dbg=dbg, status=status)
    t = mytime() - t
    prt("Connected components: {}".format(cc))
    prt("Time: {:.3f} sec".format(t))


if __name__ == "__main__":
    import doctest
    doctest.testmod()

    import sys
    files = []
    dbg = False
    status = False
    props_prefix = None
    collect = 0

    for i in sys.argv[1:]:
        if i[:1] == "-":
            if i == "-D":
                dbg = True
            elif i == "-S":
                status = True
            elif i == "-P":
                collect = 1
            else:
                prt("Unrecognized parameter {}".format(i))
        elif collect == 0:
            files.append(i)
        else:
            if collect == 1:
                props_prefix = i
            collect = 0
    if files:
        main(files, dbg=dbg, status=status, props_prefix=props_prefix)
