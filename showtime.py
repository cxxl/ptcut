
# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

comp = {}
speedups = []

def geoavg(l):
   if len(l) == 0:
      return 0.0
   p = 1
   for i in l:
      p *= i
   return p ** (1.0 / len(l))


def printit(prt, d):
   if d["model"]:
      speedup = " " * 5 + "-"
      global comp
      total = d["time"] + (d["ttime"] + d["ctime"] if total_time else 0)
      if d["model"] in comp and comp[d["model"]] >= 0.001 and d["solutions"] > 0:
         fact = comp[d["model"]] / total if total > 0 else 0
         speedup = "{:6.2f}".format(fact)
         if fact > 0:
            global speedups
            speedups.append(fact)
      if prt:
         import math
         if f_vector:
            xxx = d["f-vector"]
         else:
            xxx = "{:g}".format(float(d["combos"])) if float_combo else "10^{}".format(int(math.log10(d["combos"]))) if d["combos"] > 1 else d["combos"]
         print("{:<26s} {:3d} {:4d} {:4d} {:5d} {:8.3f}  {} {:3}  {}".format(d["model"], d["dim"], d["formulas"], d["solutions"], d["max"], total, speedup, "." if d["verified"] == -1 else "ok" if d["verified"] else "ERR", xxx))
      if not d["model"] in comp:
         comp[d["model"]] = total

def filter_speeds(f, prt):
   header = None
   d = {}
   d["model"] = None
   for line in f:
      if line.startswith("Solving "):
         printit(prt, d)
         arr = line.split()
         newheader = " ".join(arr[3:])
         if header != newheader:
            header = newheader
            if prt:
               print("Parameters: {}".format(header))
         d["model"] = arr[1]
         d["combos"] = -1
         d["dim"] = -1
         d["formulas"] = -1
         d["solutions"] = -1
         d["max"] = -1
         d["time"] = -1
         d["verified"] = -1
         d["ttime"] = 0
         d["ctime"] = 0
         d["f-vector"] = ""
      elif line.startswith("Worst case combinations:") or line.startswith("Possible combinations:"):
         # Possible combinations: 2 * 2 * 2 * 2 * 3 * 3 * 3 * 3 * 3 * 3 * 4 * 4 * 12  = 2,239,488 (10^6)
         arr = line.split("=")[1].strip().split()
         d["combos"] = int(arr[0].replace(",",""))
      elif line.startswith("Dimension:"):
         # Dimension: 12
         arr = line.split()
         d["dim"] = int(arr[1])
      elif line.startswith("Formulae:") or line.startswith("Formulas:"):
         # Formulas: 12
         arr = line.split()
         d["formulas"] = int(arr[1])
      elif line.startswith("Solutions: "):
         # Solutions: 2, dim=3.0, hs=15.0, max=8
         arr = line.split()
         d["solutions"] = int(arr[1][:-1])
         d["max"] = int(arr[-1].split("=")[1])
      elif line.startswith("f-vector: "):
         # f-vector: [0, 3, 1]
         arr = line.split(maxsplit=1)
         d["f-vector"] = arr[1].strip()
      elif line.strip() == "There is no solution!":
         d["solutions"] = 0
      elif line.startswith("Intersection time:") or line.startswith("Total time:"):
         # Intersection time: 29.006163 sec, total intersections: 2,429, total inclusion tests: 22,259
         arr = line.split()
         d["time"] = float(arr[2])
      elif line.startswith("Tropicalization time:"):
         # Tropicalization time: 0.103188 sec
         arr = line.split()
         d["ttime"] = float(arr[2])
      elif line.startswith("Creation time:"):
         # Creation time: 0.043 sec
         arr = line.split()
         d["ctime"] = float(arr[2])
      elif line.startswith("Solutions "):
         # Solutions match Satya's solutions
         d["verified"] = int(line.strip() in ("Solutions match Satya's solutions", "Solutions match known solutions", "Solutions match known solutions."))
   printit(prt, d)


if __name__ == "__main__":
   import sys

   global total_time, float_combo
   total_time = False
   float_combo = False
   f_vector = False
   files = []
   for i in sys.argv[1:]:
      if i.startswith("-"):
         if i == "-t":
            total_time = True
         elif i == "-c":
            float_combo = True
         elif i == "-f":
            f_vector = True
      else:
         files.append(i)

   if len(files) < 1:
      print(
"""
usage: 1. showtime [options] <log-file>
       2. showtime [options] <base-log-file> <new-log-file>

1.: Show time, dimension, number of formulas and number of prevarieties of
calculations in <log-file>.

2.: Show same information as in 1. for <new-log-file> and use data from
<base-log-file> as base for comparison.

options:
   -t   count the time for tropicalization and polytope creation as well""")
      sys.exit(1)

   with open(files[0]) as f:
      filter_speeds(f, len(files) == 1)

   if len(files) > 1:
      print()
      with open(files[1]) as f:
         filter_speeds(f, True)
      #avg = len(speedups) / sum([1 / i for i in speedups])
      avg = geoavg(speedups)
      print("avg speedup: {:.2f}".format(avg))
