#! /usr/bin/env python3

# $+HEADER$
#
# Copyright 2017-2020 Christoph Lueders
#
# This file is part of the PtCut project: <http://wrogn.com/ptcut>
#
# PtCut is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PtCut is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with PtCut.  If not, see <http://www.gnu.org/licenses/>.
#
# $-HEADER$

from __future__ import print_function, division

import numpy as np
from prt import prt
from math import log10, floor
from sym_util import inf
import sys
import re


#from os import times
#import resource

#def mytime():
#    return clock()
#    #t = os.times()
#    #return t[0] + t[1]
#    #r = resource.getrusage(0)
#    #return r[0] + r[1]

try:
    from time import perf_counter
    mytime = perf_counter
except ImportError:
    from time import clock
    mytime = clock

# using time.clock():
#    min=0.000 avg=0.863 max=38.000, dev=0.937 usec
# using os.times():
#    min=0.000 avg=1.100 max=10000.000, dev=104.876 usec
# using resource.getrusage():
#    min=0.000 avg=1.600 max=4000.000, dev=79.984 usec


def prod(s):
    """
    >>> prod([1, 2, 3])
    6
    >>> prod([])
    1
    >>> prod([1, -1, 2, -2])
    4
    """
    r = 1
    for i in s:
        r *= i
    return r

avg = np.mean


def myround(v, n=0):
    """
    >>> myround(0.5)
    1
    >>> myround(1.5)
    2
    >>> myround(-0.5)
    -1
    >>> myround(-1.5)
    -2
    >>> myround(0.05, 1)
    0.1
    >>> myround(0.15, 1)
    0.2
    >>> myround(5, -1)
    10
    >>> myround(15, -1)
    20
    >>> myround(78124.99999999997, -5)
    100000
    >>> myround(24.999999999999996, -1)
    30
    >>> myround(inf)
    inf
    >>> myround(0.0000005, 6)
    1e-06
    """
    sign = -1 if v < 0 else 1
    v *= sign
    if v == inf:
        return v*sign
    # handle FP-inaccuracy of 1 / 1e-05 == 99999.99999999999 (!)
    if n >= 0:
        scale = 10 ** n
        # handle FP-inaccuracy of 0.2 ** -2 == 24.999999999999996 (!)
        r = floor(v * scale + 0.5000000000000005) / scale
    else:
        scale = 10 ** -n
        r = floor(v / scale + 0.5000000000000005) * scale
    if type(v) == int or n <= 0:
        r = int(r)
    r *= sign
    return r


def toInt(a):
    return [[Integer(i) for i in v] for v in a]


_prime_add = [4, 1, 1, 9, 7, 3, 3, 19, 7, 7, 19, 3, 39, 37, 31, 37, 61, 3, 3, 51, 39, 117, 9, 117, 7, 13, 67, 103, 331, 319,
    57, 33, 49, 61, 193, 69, 67, 43, 133, 3, 121, 109, 63, 57, 31, 9, 121, 33, 193, 9, 151, 121, 327,
    171, 31, 21, 3, 279, 159, 19, 7, 93, 447, 121, 57, 49, 49, 49, 99, 9, 33, 273, 39, 79, 207, 129,
    133, 21, 93, 49, 129, 13, 391, 27, 261, 103, 151, 373, 181, 31, 289, 79, 399, 153, 97, 151, 127,
    469, 49, 289, 267, 3, 117, 129, 267, 3, 79, 3, 19, 457, 7, 139, 207, 99, 271, 79, 93, 279, 709,
    69, 79, 87, 1119, 3, 753, 237, 679, 283, 387, 459, 1113, 63, 169, 21, 7, 1143, 91, 283, 273, 513,
    13, 129, 13, 81, 91, 73, 9, 987, 247, 183, 67, 901, 13, 87, 453, 189, 451, 583, 151, 187, 303, 403,
    133, 217, 1527, 559, 267, 27, 1323, 37, 63, 193, 441, 337, 691, 339, 151, 723, 261, 979, 313, 217,
    231, 999, 37, 927, 721, 163, 183, 181, 253, 57, 1153, 357, 951, 21, 39, 1107, 553, 153, 357]

def make_prime(dig):
    r = 10 ** dig + _prime_add[dig]
    assert log10(r) >= dig and log10(r) < dig+1, "dig={}, r={}, log10(r)={}".format(dig, r, log10(r))
    return r


class status_print:
    """
    Do status prints, i.e., print over old statuses to implement counting up without
    consuming screen space.
    """
    def __init__(self):
        self.e = 0
    def print(self, text):
        ps = "\b" * self.e + text
        if len(text) < self.e:
            ps += " " * (self.e - len(text)) + "\b" * (self.e - len(text))
        print(end=ps)
        sys.stdout.flush()
        self.e = len(text)


progress = True

def set_progress(p):
    global progress
    progress = p


class tame_it:
    """
    Do something only every x seconds, like printing a status.
    """
    def __init__(self, timeout):
        self.to = timeout
        self.t1 = mytime()
    def go(self):
        if progress:
            t2 = mytime()
            if t2 - self.t1 >= self.to:
                self.t1 = t2
                return True
        return False


def time_prec():
    prt(end="\nMeasuring timer precision...", flush=True)
    # measure it
    runs = 100000
    val = [0] * runs
    for i in range(runs):
        val[i] = mytime()
    # calculate on it
    last = val[0]
    mindiff = 999
    maxdiff = 0
    sum = 0
    sum2 = 0
    cnt = 0
    for t in val[1:]:
        diff = (t - last) * 1e6
        if diff > 0:
            mindiff = min(mindiff, diff)
        maxdiff = max(maxdiff, diff)
        sum += diff
        sum2 += diff * diff
        cnt += 1
        last = t
    avg = sum / cnt
    dev = (sum2 / cnt - (sum/cnt) ** 2) ** 0.5
    prt("\nTimer precision: min={:.3f} avg={:.3f} max={:.3f} dev={:.3f} usec".format(mindiff, avg, maxdiff, dev), flush=True)


def int_or_float(s):
    """
    >>> int_or_float("1")
    1
    >>> int_or_float("-1")
    -1
    >>> int_or_float("1000000000000000000000000000")
    1000000000000000000000000000
    >>> int_or_float("1.0")
    1.0
    >>> int_or_float("1e2")
    100.0
    """
    try:
        return int(s)
    except ValueError:
        return float(s)


def int_with_unit(s):
    """
    >>> int_with_unit("1")
    1
    >>> int_with_unit("1K")
    1024
    >>> int_with_unit("1m")
    1048576
    >>> int_with_unit("0.2g")
    214748364
    >>> int_with_unit("1e2t")
    109951162777600
    >>> int_with_unit("-1p")
    -1125899906842624
    >>> int_with_unit("100.6")
    100
    """
    s = s.strip().upper()
    if s.endswith("K"):
        return int(int_or_float(s[:-1]) * 1024)
    if s.endswith("M"):
        return int(int_or_float(s[:-1]) * 1024 * 1024)
    if s.endswith("G"):
        return int(int_or_float(s[:-1]) * 1024 * 1024 * 1024)
    if s.endswith("T"):
        return int(int_or_float(s[:-1]) * 1024 * 1024 * 1024 * 1024)
    if s.endswith("P"):
        return int(int_or_float(s[:-1]) * 1024 * 1024 * 1024 * 1024 * 1024)
    return int(int_or_float(s))


def with_units(x, mindigs=2):
    """
    >>> with_units(100000, 0)
    '98K'
    >>> with_units(1000000, 0)
    '977K'
    >>> with_units(10000000)
    '9766K'
    >>> with_units(100000000)
    '95M'
    >>> with_units(10 * 1024**2)
    '10M'
    >>> with_units(10 * 1024**2 - 1)
    '10240K'
    """
    mindigs = max(1, mindigs)
    x2 = x / 10**(mindigs - 1)
    if x2 >= 1024 ** 5:
        unit = "P"
        x /= 1024 ** 5
    elif x2 >= 1024 ** 4:
        unit = "T"
        x /= 1024 ** 4
    elif x2 >= 1024 ** 3:
        unit = "G"
        x /= 1024 ** 3
    elif x2 >= 1024 ** 2:
        unit = "M"
        x /= 1024 ** 2
    elif x2 >= 1024:
        unit = "K"
        x /= 1024
    else:
        unit = ""
    return str(int(x + 0.5)) + unit


def get_isodatetime():
    import datetime, time
    # Calculate the offset taking into account daylight saving time
    utc_offset_sec = time.altzone if time.localtime().tm_isdst else time.timezone
    utc_offset = datetime.timedelta(seconds=-utc_offset_sec)
    return datetime.datetime.now().replace(tzinfo=datetime.timezone(offset=utc_offset), microsecond=0).isoformat()


if __name__ == "__main__":
    import doctest
    doctest.testmod()
